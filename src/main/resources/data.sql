/*ADDRESS*/
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'New York', 'USA', 'street 1', '82211');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'Barcelona', 'SPAIN', 'street 2', '08915');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'Los Ángeles', 'USA', 'street 3', '82222');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'Madrid', 'SPAIN', 'street 4', '08823');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'Bilbao', 'SPAIN', 'street 5', '96548');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'Chicago', 'USA', 'street 6', '99501');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'Seattle', 'USA', 'street 7', '72201');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'San Francisco', 'USA', 'street 8', '85001');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'Chicago', 'USA', 'street 9', '65018');
INSERT INTO ADDRESS_ENTITY (id, city, country, street, zip_code) VALUES (NEXTVAL('hibernate_sequence'), 'San Diego', 'USA', 'street 0', '61616');

/*USERS (password=password)*/
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Jhon', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'jhon',1);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Mark', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'mark',2);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Jessica', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'jess',3);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Maria', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'maria',4);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Betty', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'betty',5);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Chris', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'chris',6);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Benjamin', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'benjamin',7);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Jack', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'jack',8);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Annie', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'annie',9);
INSERT INTO USER_ENTITY (id, name, password, username, address_id) VALUES (NEXTVAL('hibernate_sequence'),'Daniel', '$2a$10$qivU6ZAr/g9C7bR49RyC9e5BV1/bBUmWOWOdBdnAu7JA9HFIYq1Gi', 'daniel',10);

/*ROLES*/
INSERT INTO roles (id, name) VALUES (NEXTVAL('hibernate_sequence'), 'ROLE_USER');

/*ROLES->USER*/
INSERT INTO user_roles (user_id, role_id) VALUES (11,21);
INSERT INTO user_roles (user_id, role_id) VALUES (12,21);
INSERT INTO user_roles (user_id, role_id) VALUES (13,21);
INSERT INTO user_roles (user_id, role_id) VALUES (14,21);
INSERT INTO user_roles (user_id, role_id) VALUES (15,21);
INSERT INTO user_roles (user_id, role_id) VALUES (16,21);
INSERT INTO user_roles (user_id, role_id) VALUES (17,21);
INSERT INTO user_roles (user_id, role_id) VALUES (18,21);
INSERT INTO user_roles (user_id, role_id) VALUES (19,21);
INSERT INTO user_roles (user_id, role_id) VALUES (20,21);

/*TODOs*/
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor module users', 11);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Lorem ipsum dolor sit amet.', 11);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module posts', 12);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Lorem ipsum dolor sit amet.', 12);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Vestibulum ante ipsum', 12);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'consectetur, adipisci velit...', 11);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Lorem ipsum dolor sit amet.', 11);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Lorem ipsum dolor sit dolor.', 12);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module posts', 12);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Sed porttitor dignissim', 11);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module management', 13);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module management', 13);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Lorem ipsum dolor sit amet.', 14);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Integer non purus augue', 14);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Vestibulum ante ipsum.', 14);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Curabitur consectetur', 13);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Neque porro quisquam est qui dolorem ', 13);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Cras eget pretium nisi', 15);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Donec malesuada risus', 15);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Vestibulum ante purus', 15);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Ut fermentum quis', 13);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Vestibulum ante purus', 14);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module marketing', 14);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor module human resources', 16);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module accouting', 17);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor module human resources', 16);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor module statistics', 15);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module purchasing', 16);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Proin risus erat', 15);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Neque porro quisquam est qui dolorem ', 16);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module human resources', 16);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Neque quisquam est qui ', 20);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor module security', 18);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Add exceptions', 18);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Neque porro quisquam est qui dolorem ', 18);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor services', 17);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor layout statistics', 20);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Mauris porta enim gravida', 17);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Donec a lorem nec mi ', 17);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Implements Role admin', 18);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor module finance', 18);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Deploy backend in Heroku', 19);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Consectetur, adipisci velit...', 19);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module security', 19);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'consectetur, adipisci velit...', 20);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Refactor module research', 19);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Neque porro quisquam est qui dolorem ', 17);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), false, 'Neque porro quisquam est qui dolorem ', 19);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Refactor layout home', 20);
INSERT INTO TODO_ENTITY (id, completed, title, user_id) VALUES (NEXTVAL('hibernate_sequence'), true, 'Consectetur, adipisci velit...', 20);


