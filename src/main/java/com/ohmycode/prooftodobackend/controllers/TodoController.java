package com.ohmycode.prooftodobackend.controllers;

import com.ohmycode.prooftodobackend.errors.todo.TodoConstrainValidationException;
import com.ohmycode.prooftodobackend.models.entities.TodoEntity;
import com.ohmycode.prooftodobackend.utils.PaginationLinksUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.ohmycode.prooftodobackend.errors.todo.TodoNotFoundException;
import com.ohmycode.prooftodobackend.models.dto.todo.CreateUpdateTodoDto;
import com.ohmycode.prooftodobackend.models.services.TodoService;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600, exposedHeaders = "*")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/todos")
public class TodoController {

    private final TodoService todoService;
    private final PaginationLinksUtils paginationLinksUtils;

    @GetMapping("/")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok()
                .body(todoService.findAll());
    }

    @GetMapping("/page")
    public ResponseEntity<?> getAllPageable(@PageableDefault(size = 10, page = 0) Pageable pageable,
                                            HttpServletRequest request) {
        Page<TodoEntity> result = todoService.findAll(pageable);
        if (result.isEmpty()) {
            throw new TodoNotFoundException();
        } else {
            UriComponentsBuilder uriBuilder =
                    UriComponentsBuilder.fromHttpUrl(request.getRequestURL().toString());
            return ResponseEntity.ok().header("link", paginationLinksUtils.createLinkHeader(result, uriBuilder)).body(result);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") int id) {
        return ResponseEntity.ok()
                .body(todoService.findById(id)
                        .orElseThrow(() -> new TodoNotFoundException(id)));
    }

    @GetMapping("/userid/{id}")
    public ResponseEntity<?> getByUserId(@PathVariable("id") int id) {
        return ResponseEntity.ok()
                .body(todoService.getByUserID(id));
    }

    @GetMapping("/page/userid/{id}")
    public ResponseEntity<?> getByUserIdPageable(@PageableDefault(size = 10, page = 0) Pageable pageable,
                                                 HttpServletRequest request, @PathVariable("id") int id) {
        Page<TodoEntity> result = todoService.getByUserID(id, pageable);
        if (result.isEmpty()) {
            throw new TodoNotFoundException();
        } else {
            UriComponentsBuilder uriBuilder =
                    UriComponentsBuilder.fromHttpUrl(request.getRequestURL().toString());
            return ResponseEntity.ok().header("link", paginationLinksUtils.createLinkHeader(result, uriBuilder)).body(result);
        }
    }

    @PostMapping("/")
    public ResponseEntity<?> insert(@RequestBody @Valid CreateUpdateTodoDto dto, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new TodoConstrainValidationException(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(todoService.insert(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody @Valid CreateUpdateTodoDto dto, BindingResult bindingResult,
                                    @PathVariable("id") int id,
                                    HttpServletRequest request) {

        if (bindingResult.hasErrors()) {
            throw new TodoConstrainValidationException(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }

        return ResponseEntity.ok().body(todoService.update(dto, id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id, HttpServletRequest request) {
        todoService.delete(id, request);
        return ResponseEntity.noContent().build();
    }

}
