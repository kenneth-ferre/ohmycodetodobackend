package com.ohmycode.prooftodobackend.security.controllers;

import com.ohmycode.prooftodobackend.security.models.dto.login.LoginDto;
import com.ohmycode.prooftodobackend.security.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

	private final AuthService authService;

	@PostMapping("/login")
	public ResponseEntity<?> login(@Validated @RequestBody LoginDto loginDto) {
		return ResponseEntity.ok(authService.login(loginDto));
	}

}
