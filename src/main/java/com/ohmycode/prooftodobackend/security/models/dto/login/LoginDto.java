package com.ohmycode.prooftodobackend.security.models.dto.login;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LoginDto {
    private String username;
    private String password;
}
