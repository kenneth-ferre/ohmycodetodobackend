package com.ohmycode.prooftodobackend.security.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ohmycode.prooftodobackend.models.entities.AddressEntity;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter @Setter
public class UserEntity {
	@Id	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Length(min = 1, max = 199)
	@Column(unique = true)
	private String username;

	@Length(min = 1, max = 199)
	@JsonIgnore
	private String password;

	@Length(min = 1, max = 199)
	@Column(nullable = false)
	private String name;

	@ManyToOne
	@JoinColumn(name = "address_id")
	AddressEntity addressEntity;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_roles",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	public UserEntity() {	}

	public UserEntity(String username, String name, String password, AddressEntity addressEntity) {
		this.username = username;
		this.name = name;
		this.addressEntity = addressEntity;
		this.password = password;
	}

}
