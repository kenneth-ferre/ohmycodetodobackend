package com.ohmycode.prooftodobackend.security.services;

import com.ohmycode.prooftodobackend.errors.user.CustomUserNotFoundException;
import com.ohmycode.prooftodobackend.security.models.entity.UserEntity;
import com.ohmycode.prooftodobackend.security.repos.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public UserEntity findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(CustomUserNotFoundException::new);
    }

    public UserEntity findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(CustomUserNotFoundException::new);
    }

    public List<UserEntity> getAll(){
        return userRepository.findAll();
    }

}
