package com.ohmycode.prooftodobackend.models.entities;

import com.ohmycode.prooftodobackend.security.models.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity @Data
@AllArgsConstructor @NoArgsConstructor
public class TodoEntity {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private boolean completed;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;
}
