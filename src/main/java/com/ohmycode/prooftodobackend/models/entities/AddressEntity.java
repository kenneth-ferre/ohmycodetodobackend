package com.ohmycode.prooftodobackend.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity @Data
@AllArgsConstructor @NoArgsConstructor
public class AddressEntity {
    @Id @GeneratedValue
    private Integer id;

    @Length(min = 1, max = 199)
    private String street;

    @Length(min = 1, max = 199)
    private String city;

    @Length(min = 1, max = 199)
    private String zipCode;

    @Length(min = 1, max = 199)
    private String country;
}
