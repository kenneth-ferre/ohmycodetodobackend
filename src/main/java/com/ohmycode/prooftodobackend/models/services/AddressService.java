package com.ohmycode.prooftodobackend.models.services;

import com.ohmycode.prooftodobackend.models.entities.AddressEntity;
import com.ohmycode.prooftodobackend.models.repos.AddressRepo;
import com.ohmycode.prooftodobackend.models.services.base.BaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AddressService extends BaseService<AddressEntity, Integer, AddressRepo> {
}
