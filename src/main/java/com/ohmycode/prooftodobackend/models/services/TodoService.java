package com.ohmycode.prooftodobackend.models.services;

import com.ohmycode.prooftodobackend.errors.todo.TodoCanNotModifyByOtherUserException;
import com.ohmycode.prooftodobackend.errors.todo.TodoConstrainValidationException;
import com.ohmycode.prooftodobackend.errors.todo.TodoNotFoundException;
import com.ohmycode.prooftodobackend.models.dto.todo.CreateUpdateTodoDto;
import com.ohmycode.prooftodobackend.models.entities.TodoEntity;
import com.ohmycode.prooftodobackend.models.repos.TodoRepo;
import com.ohmycode.prooftodobackend.models.services.base.BaseService;
import com.ohmycode.prooftodobackend.security.models.entity.UserEntity;
import com.ohmycode.prooftodobackend.security.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequiredArgsConstructor
@Service
public class TodoService extends BaseService<TodoEntity, Integer, TodoRepo> {
    private final UserService userService;

    public TodoEntity insert(CreateUpdateTodoDto dto) {
        UserEntity user = userService.findById(dto.getUserId());
        TodoEntity todoEntity = new TodoEntity();
        todoEntity.setUser(user);
        todoEntity.setTitle(dto.getTitle());
        todoEntity.setCompleted(dto.isCompleted());
        return repositorio.save(todoEntity);
    }

    public TodoEntity update(CreateUpdateTodoDto dto, Integer id, HttpServletRequest request) {

        UserEntity userRequest = userService.findByUsername(request.getRemoteUser());
        UserEntity userOwner = userService.findById(dto.getUserId());

        if ( checkIfCanModify(userOwner, userRequest) ) {

            TodoEntity todoEntity = repositorio.findById(id).orElseThrow(()->new TodoNotFoundException(id));
            todoEntity.setTitle(dto.getTitle());
            todoEntity.setCompleted(dto.isCompleted());
            return repositorio.save(todoEntity);

        } else {
            throw new TodoCanNotModifyByOtherUserException();
        }


    }

    public void delete(int id, HttpServletRequest request) {

        UserEntity userRequest = userService.findByUsername(request.getRemoteUser());
        TodoEntity todoEntity = repositorio.findById(id).orElseThrow(()->new TodoNotFoundException(id));
        UserEntity userOwner = userService.findById(todoEntity.getUser().getId());

        if ( checkIfCanModify(userOwner, userRequest) ) {
            repositorio.delete(todoEntity);
        } else {
            throw new TodoCanNotModifyByOtherUserException();
        }

    }

    public List<TodoEntity> getByUserID(int id) {
        List<TodoEntity> todoEntityList = repositorio.findByUser_Id((long) id);
        if (todoEntityList.size() > 0) {
            return todoEntityList;
        } else {
            throw new TodoNotFoundException((long) id);
        }
    }

    public Page<TodoEntity> getByUserID(int id, Pageable pageable) {
        Page<TodoEntity> todoEntityList = repositorio.findByUser_Id((long) id, pageable);
        if (todoEntityList.getSize() > 0) {
            return todoEntityList;
        } else {
            throw new TodoNotFoundException((long) id);
        }
    }

    boolean checkIfCanModify(UserEntity userOwner, UserEntity userRequest){
        return userOwner.getId().equals(userRequest.getId());
    }
}
