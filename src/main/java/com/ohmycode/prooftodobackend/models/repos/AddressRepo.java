package com.ohmycode.prooftodobackend.models.repos;

import com.ohmycode.prooftodobackend.models.entities.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepo extends JpaRepository<AddressEntity, Integer> {
}
