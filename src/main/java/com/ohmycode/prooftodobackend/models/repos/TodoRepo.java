package com.ohmycode.prooftodobackend.models.repos;

import com.ohmycode.prooftodobackend.models.entities.TodoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface TodoRepo extends JpaRepository<TodoEntity, Integer> {

    List<TodoEntity> findByUser_Id(Long id);

    Page<TodoEntity> findByUser_Id(Long id, Pageable pageable);

}
