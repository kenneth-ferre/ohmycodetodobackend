package com.ohmycode.prooftodobackend.models.dto.todo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Validated
@Getter @Setter
public class CreateUpdateTodoDto {
    private Long userId;

    @NotNull(message = "The TODO title must not be null")
    @Size(min=1, max=199, message = "Title size too large, maximum < 200")
    private String title;

    private boolean completed;
}
