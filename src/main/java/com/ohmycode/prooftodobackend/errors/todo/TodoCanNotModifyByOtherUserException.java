package com.ohmycode.prooftodobackend.errors.todo;

public class TodoCanNotModifyByOtherUserException extends RuntimeException{

    public TodoCanNotModifyByOtherUserException() { super( "Only the owner can modify their TODO");}


}
