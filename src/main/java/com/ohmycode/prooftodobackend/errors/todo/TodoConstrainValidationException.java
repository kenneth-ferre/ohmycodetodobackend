package com.ohmycode.prooftodobackend.errors.todo;

public class TodoConstrainValidationException extends RuntimeException{

    public TodoConstrainValidationException(String message) { super( message);}

}
