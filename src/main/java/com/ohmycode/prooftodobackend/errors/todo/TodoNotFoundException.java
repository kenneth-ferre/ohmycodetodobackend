package com.ohmycode.prooftodobackend.errors.todo;

public class TodoNotFoundException extends RuntimeException{

    public TodoNotFoundException() { super( "TODO's not found");}

    public TodoNotFoundException(int id) { super( "TODO with id: " + id + " not found"); }

    public TodoNotFoundException(Long id) {
        super( "TODO's by user id: " + id + " not found");
    }
}
