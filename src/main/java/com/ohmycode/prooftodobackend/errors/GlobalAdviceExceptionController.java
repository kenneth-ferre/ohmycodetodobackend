package com.ohmycode.prooftodobackend.errors;

import com.ohmycode.prooftodobackend.errors.todo.TodoCanNotModifyByOtherUserException;
import com.ohmycode.prooftodobackend.errors.todo.TodoConstrainValidationException;
import com.ohmycode.prooftodobackend.errors.todo.TodoNotFoundException;
import com.ohmycode.prooftodobackend.errors.user.CustomUserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalAdviceExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(TodoNotFoundException.class)
    public ResponseEntity<ApiError> handleUsernameIsTaken(TodoNotFoundException ex) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getMessage());
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }

    @ExceptionHandler(TodoCanNotModifyByOtherUserException.class)
    public ResponseEntity<ApiError> handleCanModify(TodoCanNotModifyByOtherUserException ex) {
        ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getMessage());
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }

    @ExceptionHandler(TodoConstrainValidationException.class)
    public ResponseEntity<ApiError> handleCanModify(TodoConstrainValidationException ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage());
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }

    @ExceptionHandler(CustomUserNotFoundException.class)
    public ResponseEntity<ApiError> handleUserNotFound(CustomUserNotFoundException ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage());
        return ResponseEntity.status(apiError.getStatus()).body(apiError);

    }

}