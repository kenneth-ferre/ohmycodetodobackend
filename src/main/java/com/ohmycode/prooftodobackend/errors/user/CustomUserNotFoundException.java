package com.ohmycode.prooftodobackend.errors.user;

public class CustomUserNotFoundException extends RuntimeException{
    public CustomUserNotFoundException() { super( "user not found");}
}
