package com.ohmycode.prooftodobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProofTodoBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProofTodoBackendApplication.class, args);
    }

}
