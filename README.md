# Ohmycodetodo Backend

## Requirements

For building and running the application you need:

- [Java Development Kit]
- [Maven]

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `proofTodoBackend\src\main\java\com\ohmycode\prooftodobackend\ProofTodoBackendApplication.java` class from your IDE.
proofTodoBackend\src\main\java\com\ohmycode\prooftodobackend\ProofTodoBackendApplication.java

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Requests
https://documenter.getpostman.com/view/11620031/Tzm6jv1e
